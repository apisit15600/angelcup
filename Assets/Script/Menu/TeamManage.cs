﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TeamManage : MonoBehaviour {
   
   public static TeamManage Instance {get; private set ;} 
    public int selectedTeamIndex;
    [Header ("List of Teams")]
    [SerializeField] public List<TeamObject> teamList = new List<TeamObject> ();
    [Header ("UI References")]
    [SerializeField] public TextMeshProUGUI teamname;
    [SerializeField] public Image teamSplash;
    [SerializeField] Button _ConfirmButton;
    public void Awake()
    {
        if(Instance == null)
        {
            Instance = this; 
            DontDestroyOnLoad(gameObject);
        }else
        {
            Destroy(gameObject);
        }
    }
    public void Start () {
        _ConfirmButton.onClick.AddListener (
            delegate { ConfirmButtonClick (_ConfirmButton); });
        UpdateTeamSelection ();
    }
    public void LeftArrow () {
        selectedTeamIndex--;
        if (selectedTeamIndex < 0)
            selectedTeamIndex = teamList.Count - 1;
        UpdateTeamSelection ();
    }
    public void RightArrow () {
        selectedTeamIndex++;
        if (selectedTeamIndex == teamList.Count)
            selectedTeamIndex = 0;
        UpdateTeamSelection ();
    }
    public void ConfirmButtonClick (Button button) {
        if (selectedTeamIndex == 0) {
            SceneManager.LoadScene ("SelectMode");
            Debug.Log (" SelectTeam : " + selectedTeamIndex);
        } 
        if (selectedTeamIndex == 1) {
            SceneManager.LoadScene ("SelectMode");
            Debug.Log (" SelectTeam : " + selectedTeamIndex);

        }
        if (selectedTeamIndex == 2) {
           SceneManager.LoadScene ("SelectMode");
            Debug.Log (" SelectTeam : " + selectedTeamIndex);
            
        }
        if (selectedTeamIndex == 3) {
            SceneManager.LoadScene ("SelectMode");
            Debug.Log (" SelectTeam : " + selectedTeamIndex);

        }
    }
    public void UpdateTeamSelection () {
        teamSplash.sprite = teamList[selectedTeamIndex].splash;
        teamname.text = teamList[selectedTeamIndex].teamname;
    }

    [System.Serializable]
    public class TeamObject {
        public Sprite splash;
        public string teamname;
    }

}