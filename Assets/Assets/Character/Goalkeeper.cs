﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goalkeeper : MonoBehaviour {
    Vector3 _position;
    int direc;
    public float speed;

    bool left = true;
    // Start is called before the first frame update
    void Start () {
        _position = transform.position;
        if(SelectMode.Instance.c == 1)
        {
            speed = 0.1f ;
        }
        if(SelectMode.Instance.c == 2)
        {
            speed = 0.3f ;
        }
        if(SelectMode.Instance.c == 3)
        {
            speed = 0.5f ;
        }
    }

    // Update is called once per frame
    void Update () {

        if (left == true) {
            direc = 1;
        } else if(left == false) {

            direc = -1;
        }

        transform.Translate (Vector3.left * speed * direc);

        if(transform.position.x >=16){
           left = false;
        }
        else if(transform.position.x <= -16){
            left = true;
        }

    }
}