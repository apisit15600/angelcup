﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Pause : MonoBehaviour {
        public GameObject PauseMenu;
        [SerializeField] Button _resumeButton;
        [SerializeField] Button _mainButton;
        [SerializeField] Button _exitButton;
        // Start is called before the first frame update
        void Start () {
            _mainButton.onClick.AddListener (
                delegate { MainButtonClick (_mainButton); });
            _exitButton.onClick.AddListener (
                delegate { ExitButtonClick (_exitButton); });
            _resumeButton.onClick.AddListener (
                delegate { ResumeButtonClick (_resumeButton); });
        }

        // Update is called once per frame
        void Update ()
         {
            if (Input.GetKeyDown (KeyCode.Escape)) 
            {
                if (PauseMenu != null) {
                    PauseMenu.SetActive (true);
                }
            }
         }

            public void MainButtonClick (Button button) {
                SceneManager.LoadScene ("SecenMainmenu");
            }
            public void ResumeButtonClick (Button button) {
                PauseMenu.SetActive (false);
            }
            public void ExitButtonClick (Button button) {
                Application.Quit ();
            }

        }