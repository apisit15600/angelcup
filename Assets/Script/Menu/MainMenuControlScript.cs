﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuControlScript : MonoBehaviour {
    //Declarations section
    [SerializeField] Button _startButton;
    [SerializeField] Button _howButton;
    [SerializeField] Button _optionsButton;
    [SerializeField] Button _exitButton;
    //Inside Start() method
    // Use this for initialization
    void Start () {

        _startButton.onClick.AddListener (
            delegate { StartButtonClick (_startButton); });
         _howButton.onClick.AddListener (
            delegate { HowButtonClick (_howButton); });    
        _optionsButton.onClick.AddListener (
            delegate { OptionsButtonClick (_optionsButton); });
        _exitButton.onClick.AddListener (
            delegate { ExitButtonClick (_exitButton); });
    }

    // Update is called once per frame
    void Update () { }
    public void StartButtonClick (Button button) {
        SceneManager.LoadScene ("SelectTeam");
    }
    public void HowButtonClick (Button button) {
        SceneManager.LoadScene ("SceneHowtoplay");
    } 
    public void OptionsButtonClick (Button button) {
        
            SceneManager.LoadScene ("SceneOptions");
       
    }

    public void ExitButtonClick (Button button) {
        Application.Quit ();
    }

}