﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class PlayerKick : MonoBehaviour {
    Animator kickBall ;
    Vector3 _initialPosition;
    NavMeshAgent agent;
    public Transform ball;
    [SerializeField]
    Slider _sliderPower;
    [SerializeField]
    Slider _sliderPerfect;
    // Start is called before the first frame update
    bool Selectpower = true;
    bool _bGainingUp = true;
    public float timeAnimate = 0 ;
    bool kick ;
    void Start () {
        agent = this.GetComponent<NavMeshAgent> ();
        agent.speed = 5;
        _initialPosition = this.transform.position;
        kickBall = this.GetComponent<Animator>();
        
        
    }

    // Update is called once per frame
    void Update () {

        if (Selectpower)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                _sliderPower.value = 0;

            }
            else if (Input.GetKey(KeyCode.Space))
            {

                _sliderPower.value += Time.deltaTime * 100;


                if (_bGainingUp)
                {
                    _sliderPerfect.value += Time.deltaTime * 100;

                    if (_sliderPerfect.value >= _sliderPerfect.maxValue)
                    {
                        _bGainingUp = false;
                    }
                }

                else
                {
                    _sliderPerfect.value -= Time.deltaTime * 100;
                    if (_sliderPerfect.value <= _sliderPerfect.minValue)
                    {
                        _bGainingUp = true;
                    }


                }


            }
            else if (Input.GetKeyUp(KeyCode.Space))
            {
                Selectpower = false;

                
            }
        }
        if(Selectpower == false)
        {
            

            if (Input.GetKeyUp(KeyCode.Space))
            {
                agent.SetDestination(ball.position);
                kickBall.SetBool("Kick",true);
                kick = true ;

            }
    
        }
        if(kick == true)
        {
            timeAnimate += Time.deltaTime;
            if( timeAnimate > 2)
            {
                kickBall.SetBool("Kick",false);
                kick = false ;
                timeAnimate = 0 ;
            }
        }
    }
    void OnCollisionEnter (Collision other) {

        if (other.gameObject.tag == "Ball") {
            agent.SetDestination (_initialPosition);
            this.transform.position = _initialPosition;
            Selectpower = true;
            _sliderPower.value = 0;
        }
    }
}