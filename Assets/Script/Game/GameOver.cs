﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    //Declarations section
    [SerializeField] Button _tryButton;
    [SerializeField] Button _backtoMenuButton;
    [SerializeField] Button _exitButton;
    //Inside Start() method
    // Use this for initialization
    void Start () {

        _tryButton.onClick.AddListener (
            delegate { TryButtonClick (_tryButton); });
         _backtoMenuButton.onClick.AddListener (
            delegate { BacktoMenuButtonClick (_backtoMenuButton); });    
        _exitButton.onClick.AddListener (
            delegate { ExitButtonClick (_exitButton); });
    }

    // Update is called once per frame
    void Update () { }
    public void TryButtonClick (Button button) {
        SceneManager.LoadScene ("SelectTeam");
    }
    public void BacktoMenuButtonClick (Button button) {
        SceneManager.LoadScene ("SecenMainmenu");
    } 
    public void ExitButtonClick (Button button) {
        Application.Quit ();
    }
}
