﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogoCheck_2 : MonoBehaviour
{
    public static LogoCheck_2 Instance { get; private set; }
    public Sprite[] logoteam;
    public Image logoPlayer;
    public Image logoEnemy;
    public Image logoPlayerM_2;
    public Image logoEnemyM_2;

    public GameObject[] modelPlayer;
    public GameObject[] modelGoalkeeper;
    public GameObject[] ball;
    public void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        if (TeamManage.Instance.selectedTeamIndex == 0)
        {
            logoPlayer.sprite = logoteam[0];
            logoPlayerM_2.sprite = logoteam[0];
            modelPlayer[0].SetActive(true);
            ball[0].SetActive(true);
            if (LogoCheck.Instance.randomEnemy == 1)
            {
                int randomEnemy;
                randomEnemy = Random.Range(2, 3);
                logoEnemy.sprite = logoteam[randomEnemy];
                logoEnemyM_2.sprite = logoteam[randomEnemy];
                modelGoalkeeper[randomEnemy].SetActive(true);

            }
            if (LogoCheck.Instance.randomEnemy == 2)
            {

                logoEnemyM_2.sprite = logoteam[3];
                logoEnemy.sprite = logoteam[3];
                modelGoalkeeper[3].SetActive(true);

            }
            if (LogoCheck.Instance.randomEnemy == 3)
            {
                int randomEnemy;
                randomEnemy = Random.Range(1, 2);
                logoEnemyM_2.sprite = logoteam[randomEnemy];
                logoEnemy.sprite = logoteam[randomEnemy];
                modelGoalkeeper[randomEnemy].SetActive(true);
            }
        }
        if (TeamManage.Instance.selectedTeamIndex == 1)
            {
                logoPlayer.sprite = logoteam[1];
                logoPlayerM_2.sprite = logoteam[1];
                modelPlayer[1].SetActive(true);
                ball[1].SetActive(true);
                if (LogoCheck.Instance.randomEnemy == 2)
                {
                    logoEnemyM_2.sprite = logoteam[3];
                    logoEnemy.sprite = logoteam[3];
                    modelGoalkeeper[3].SetActive(true);
                }if(LogoCheck.Instance.randomEnemy == 3)
                {
                    logoEnemyM_2.sprite = logoteam[2];
                    logoEnemy.sprite = logoteam[2];
                    modelGoalkeeper[2].SetActive(true);
                }


            }
        if (TeamManage.Instance.selectedTeamIndex == 2)
            {
                logoPlayer.sprite = logoteam[2];
                logoPlayerM_2.sprite = logoteam[2];
                modelPlayer[2].SetActive(true);
                ball[2].SetActive(true);
                if (LogoCheck.Instance.randomEnemy == 0)
                {
                    logoEnemyM_2.sprite = logoteam[1];
                    logoEnemy.sprite = logoteam[1];
                    modelGoalkeeper[1].SetActive(true);
                }
                if (LogoCheck.Instance.randomEnemy == 1)
                {
                    logoEnemyM_2.sprite = logoteam[0];
                    logoEnemy.sprite = logoteam[0];
                    modelGoalkeeper[0].SetActive(true);
                }

            }
        if (TeamManage.Instance.selectedTeamIndex == 3)
            {
                logoPlayer.sprite = logoteam[3];
                logoPlayerM_2.sprite = logoteam[3];
                modelPlayer[3].SetActive(true);
                ball[3].SetActive(true);
                if (LogoCheck.Instance.randomEnemy == 0)
                {
                    int randomEnemy;
                    randomEnemy = Random.Range(1, 2);
                    logoEnemyM_2.sprite = logoteam[randomEnemy];
                    modelGoalkeeper[randomEnemy].SetActive(true);
                }
                if (LogoCheck.Instance.randomEnemy == 1)
                {
                    modelGoalkeeper[2].SetActive(true);
                    logoEnemyM_2.sprite = logoteam[2];
                }
                if (LogoCheck.Instance.randomEnemy == 2)
                {
                    int randomEnemy;
                    randomEnemy = Random.Range(0, 1);
                    modelGoalkeeper[randomEnemy].SetActive(true);
                    logoEnemyM_2.sprite = logoteam[randomEnemy];
                }

            }
        }
    

    // Update is called once per frame
    void Update()
    {

    }

}

