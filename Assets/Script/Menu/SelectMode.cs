﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SelectMode : MonoBehaviour
{
    public static SelectMode Instance { get; private set;}
    [SerializeField] Button _easyButton;
    [SerializeField] Button _normalButton;
    [SerializeField] Button _hardButton;
    public _mode mode;
    public enum _mode { Begin ,Easy , Normal , Hard} ;
    public int c ;
    public void Awake()
    {
        if(Instance == null)
        {
            Instance = this; 
            DontDestroyOnLoad(gameObject);
        }else
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    public void Start()
    {
        _mode mode =  _mode.Begin ;
        _easyButton.onClick.AddListener (
            delegate { EasyButtonClick (_easyButton); });
        _normalButton.onClick.AddListener (
            delegate { NormalButtonClick (_normalButton); });
        _hardButton.onClick.AddListener (
        delegate { HardButtonClick (_hardButton); });
    }

    // Update is called once per frame
    public void Update()
    {
       /* switch (mode)
        {
              case _mode.Easy : SceneManager.LoadScene("Gameplay") ;
              break ;
              case _mode.Normal : SceneManager.LoadScene("Gameplay") ;
              break ;
              case _mode.Hard : SceneManager.LoadScene("Gameplay") ;
              break ;
        }*/
    }
    public void EasyButtonClick (Button button) {
        SceneManager.LoadScene("Gameplay");
        mode = _mode.Easy ;
        c = 1 ;
        Debug.Log("Easy"+c);
    }
     public void NormalButtonClick (Button button) {
        SceneManager.LoadScene("Gameplay");
        mode = _mode.Normal ;
        c = 2 ;
         Debug.Log("Normal"+c);
     }
public void HardButtonClick (Button button) {
       SceneManager.LoadScene("Gameplay");
        mode = _mode.Hard ;
         Debug.Log("Hard"+c);
         c = 3 ;
}
}
