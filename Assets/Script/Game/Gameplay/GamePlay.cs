﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePlay : MonoBehaviour
{
    
    float _currentThetaAngle = 90;
    float _currentPhiAngle = 90;
    float MaxAngle = 150;
    float MinAngle = 50;
    float MinphiAngle = 89;
    float _length = 100;
    float ANGLE_STEP = 0.5f;
    Vector3 _initialPosition;
    
    Vector3 unitCircleDirection ;
    [SerializeField]
     Slider _sliderPower;
    [SerializeField]
    Slider _sliderPerfect;
    [Header ("List of Score")]
    public Text Kick;
    int kick_Value ;
    int score_in;
    public Text _goal;
    public GameObject Score1;
    public GameObject NoScore1;
    public GameObject Score2;
    public GameObject NoScore2;
    public GameObject Score3;
    public GameObject NoScore3;
    public GameObject Score4;
    public GameObject NoScore4;
    public GameObject Score5;
    public GameObject NoScore5;
    [Header ("List of Game State")]
    public GameObject Gameover;
    public GameObject Nextstate ;
    public float timePlay = 0 ;
    bool finish ;
    float timeCollision;
    bool _collision;
    // Start is called before the first frame update
    void Start()
    {
        _initialPosition = this.transform.position;
    }


    // Update is called once per frame
    void Update()
    {
        float sine = Mathf.Sin(_currentThetaAngle * Mathf.Deg2Rad);
        float cosine = Mathf.Cos(_currentThetaAngle * Mathf.Deg2Rad);
        float sinePhi = Mathf.Sin(_currentPhiAngle * Mathf.Deg2Rad);
        float cosinePhi = Mathf.Cos(_currentPhiAngle * Mathf.Deg2Rad);
         unitCircleDirection = new Vector3(_length * cosine * sinePhi, _length * cosinePhi, _length * sine * sinePhi);


        if (Input.GetKey(KeyCode.LeftArrow))
        {
            _currentThetaAngle += ANGLE_STEP;

           if(_currentThetaAngle >= MaxAngle)
            {
                ANGLE_STEP = 0;
            }
        }


        if (Input.GetKey(KeyCode.RightArrow))
        {
            _currentThetaAngle -= ANGLE_STEP;
            
            if (_currentThetaAngle <= MinAngle)
            {
                ANGLE_STEP = 0;
            }

        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            _currentPhiAngle -= ANGLE_STEP;

         
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            _currentPhiAngle += ANGLE_STEP;

            if (_currentPhiAngle <= MinphiAngle)
            {
                _currentPhiAngle -= ANGLE_STEP;
            }
        }
        //Easy Mode
        if(kick_Value == 5 && SelectMode.Instance.c == 1 )
        {
             finish = true ;
             if(finish == true)
        {
            timePlay += Time.deltaTime ;
        
            if(score_in < 3 )
            {
                if(timePlay > 3)
                {
                Gameover.SetActive(true);
                timePlay = 0 ;
                finish = false ;
                }
            }

           if(score_in > 2 )
            {
               if(timePlay > 3){
              Nextstate.SetActive(true);
              timePlay = 0 ;
              finish = false ;
               }
            }
        }
        }
         //Nornal Mode
        if(kick_Value == 5 && SelectMode.Instance.c == 2 )
        {
             finish = true ;
             if(finish == true)
        {
            timePlay += Time.deltaTime ;
        
            if(score_in < 4 )
            {
                if(timePlay > 3)
                {
                Gameover.SetActive(true);
                timePlay = 0 ;
                finish = false ;
                }
            }

           if(score_in > 4 )
            {
               if(timePlay > 3){
              Nextstate.SetActive(true);
              timePlay = 0 ;
              finish = false ;
               }
            }
        }
        }
         //Hard Mode
        if(kick_Value == 5 && SelectMode.Instance.c == 3 )
        {
             finish = true ;
             if(finish == true)
        {
            timePlay += Time.deltaTime ;
        
            if(score_in < 5 )
            {
                if(timePlay > 3)
                {
                Gameover.SetActive(true);
                timePlay = 0 ;
                finish = false ;
                }
            }

           if(score_in > 4 )
            {
               if(timePlay > 3){
              Nextstate.SetActive(true);
              timePlay = 0 ;
              finish = false ;
               }
            }
        }
        }
        
        if(_collision == true){
            timeCollision += Time.deltaTime;
            if(timeCollision >= 1 ){
            this.transform.position = _initialPosition;
            Rigidbody rb = transform.GetComponent<Rigidbody>();
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;

            _currentThetaAngle = 90;
             _currentPhiAngle = 90;

            _sliderPower.value = 0;
            _sliderPerfect.value = 0;
            timeCollision = 0;
            _collision = false;
            }
              }


        

    }

    
    void OnCollisionEnter(Collision other){
       
       

        if (other.gameObject.tag == "Player")
      {
            Rigidbody rb = this.GetComponent<Rigidbody>();
            rb.AddForce(unitCircleDirection.normalized   * _sliderPower.value /5 ,ForceMode.Impulse);
            kick_Value += 1;
            Kick.text = "KICK : " + kick_Value.ToString() + "/5";

            if (_sliderPerfect.value > 5)
            {
                _currentThetaAngle -= 20 ;
            }
            if (_sliderPerfect.value < -5)
            {
                _currentThetaAngle += 20;
            }
            Debug.Log(" Vulue : "+_currentThetaAngle);
        }

        if(other.gameObject.tag == "Goalkeeper")
        {
            this.transform.position = _initialPosition;
            Rigidbody rb = transform.GetComponent<Rigidbody>();
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;

            _currentThetaAngle = 90;
             _currentPhiAngle = 90;

            _sliderPower.value = 0;
            _sliderPerfect.value = 0;
            timeCollision = 0;
        }
        if (other.gameObject.tag == "Goal" || other.gameObject.tag == "Wall")
        {

            
            _collision = true;

        }
        if (other.gameObject.tag == "Goal")
        {

            score_in += 1 ;
            _goal.text = "GOAL : " + score_in.ToString() + "/4";

            if (Kick.text == "KICK : 1/5")
            {
                Score1.SetActive(true);
            }
           
            if(Kick.text == "KICK : 2/5")
            {
                Score2.SetActive(true);
            }
            if (Kick.text == "KICK : 3/5")
            {
                Score3.SetActive(true);
            }
            if (Kick.text == "KICK : 4/5")
            {
                Score4.SetActive(true);
            }
            if (Kick.text == "KICK : 5/5")
            {
                Score5.SetActive(true);
            }

        }

        if (other.gameObject.tag == "Wall" || other.gameObject.tag == "Goalkeeper" )
        {
            if (Kick.text == "KICK : 1/5")
            {
                NoScore1.SetActive(true);
            }
            if (Kick.text == "KICK : 2/5")
            {
                NoScore2.SetActive(true);
            }
            if (Kick.text == "KICK : 3/5")
            {
                NoScore3.SetActive(true);
            }
            if (Kick.text == "KICK : 4/5")
            {
                NoScore4.SetActive(true);
            }
            if (Kick.text == "KICK : 5/5")
            {
                NoScore5.SetActive(true);
            }
        }
        
        


    }

}
