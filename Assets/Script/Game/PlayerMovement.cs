﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float MovementSpeed ;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.touchCount > 0 & Input.GetTouch(0).phase == TouchPhase.Moved)
        {
           
            Vector2 TouchPosition = Input.GetTouch(0).deltaPosition;
            transform.Translate(TouchPosition.x *  MovementSpeed * Time.deltaTime, 0,0);
        }
        
    }
}
