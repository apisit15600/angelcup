﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Matching : MonoBehaviour
{
    public GameObject matching;
    public float timestart = 0;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        timestart += Time.deltaTime;

        if (timestart > 5)
        {
            matching.SetActive(false);
        }

    }
}
