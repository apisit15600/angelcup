﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Nextstate_2 : MonoBehaviour
{
    public Sprite[] logoteam;
    public Image logoPlayer;
    public float timestart = 0 ; 
    // Start is called before the first frame update
    void Start()
    {
         if (TeamManage.Instance.selectedTeamIndex == 0)
        {
            logoPlayer.sprite = logoteam[0];
        }
        if (TeamManage.Instance.selectedTeamIndex == 1)
        {
            logoPlayer.sprite = logoteam[1];
        }
        if (TeamManage.Instance.selectedTeamIndex == 2)
        {
            logoPlayer.sprite = logoteam[2];
        }
        if (TeamManage.Instance.selectedTeamIndex == 3)
        {
            logoPlayer.sprite = logoteam[3];
        }
       
    }

    // Update is called once per frame
    void Update()
    {
         timestart += Time.deltaTime;
        
         if(timestart > 5 )
        {
            SceneManager.LoadScene ("trophy_Win");
        }
}
}
