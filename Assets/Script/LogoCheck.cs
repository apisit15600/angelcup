﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogoCheck : MonoBehaviour
{
    public static LogoCheck Instance { get; private set; }
    public Sprite[] logoteam;
    public Image logoPlayer;
    public Image logoEnemy;
    public float time;
    public GameObject[] modelPlayer;
    public GameObject[] modelGoalkeeper;
    public GameObject[] ball;
     public GameObject matching;
     public Image logoPlayerM;
    public Image logoEnemyM;
     public int randomEnemy;
    public void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        if (TeamManage.Instance.selectedTeamIndex == 0)
        {
            randomEnemy =  Random.Range(1, 3);
            logoPlayer.sprite = logoteam[0];
            logoEnemy.sprite = logoteam[randomEnemy];
            logoPlayerM.sprite = logoteam[0];
            logoEnemyM.sprite = logoteam[randomEnemy];
            modelPlayer[0].SetActive(true);
            ball[0].SetActive(true);
            modelGoalkeeper[randomEnemy].SetActive(true);
            Debug.Log("Enemy : " + randomEnemy);


        }
        if (TeamManage.Instance.selectedTeamIndex == 1)
        {
            
            randomEnemy = Random.Range(2, 3);
            logoPlayer.sprite = logoteam[1];
            logoEnemy.sprite = logoteam[randomEnemy];
            logoPlayerM.sprite = logoteam[1];
            logoEnemyM.sprite = logoteam[randomEnemy];
            modelPlayer[1].SetActive(true);
            ball[1].SetActive(true);
            modelGoalkeeper[randomEnemy].SetActive(true);
            Debug.Log("Enemy : " + randomEnemy);
        }
        if (TeamManage.Instance.selectedTeamIndex == 2)
        {
           
            randomEnemy = Random.Range(0, 1);
            logoPlayer.sprite = logoteam[2];
            logoEnemy.sprite = logoteam[randomEnemy];
            logoPlayerM.sprite = logoteam[2];
            logoEnemyM.sprite = logoteam[randomEnemy];
            modelPlayer[2].SetActive(true);
            ball[2].SetActive(true);
            modelGoalkeeper[randomEnemy].SetActive(true);
            Debug.Log("Enemy : " + randomEnemy);
        }
        if (TeamManage.Instance.selectedTeamIndex == 3)
        {
           
            randomEnemy = Random.Range(0, 2);
            logoPlayer.sprite = logoteam[3];
            logoEnemy.sprite = logoteam[randomEnemy];
            logoPlayerM.sprite = logoteam[3];
            logoEnemyM.sprite = logoteam[randomEnemy];
            modelPlayer[3].SetActive(true);
            ball[3].SetActive(true);
            modelGoalkeeper[randomEnemy].SetActive(true);
            Debug.Log("Enemy : " + randomEnemy);
        }

    }

    // Update is called once per frame
    void Update()
    {
    }
}

