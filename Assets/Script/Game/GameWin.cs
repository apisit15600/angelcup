﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameWin : MonoBehaviour
{
    //Declarations section
    
    [SerializeField] Button _backtoMenuButton;
    [SerializeField] Button _exitButton;
    public GameObject[] trophy ;
    //Inside Start() method
    // Use this for initialization
    void Start () {

       
         _backtoMenuButton.onClick.AddListener (
            delegate { BacktoMenuButtonClick (_backtoMenuButton); });    
        _exitButton.onClick.AddListener (
            delegate { ExitButtonClick (_exitButton); });

            if(SelectMode.Instance.c == 1)
            {
                trophy[0].SetActive(true);
            }
            if(SelectMode.Instance.c == 2)
            {
                 trophy[1].SetActive(true);
            }
            if(SelectMode.Instance.c ==3 )
            {
                 trophy[2].SetActive(true);
            }
         
    }

    // Update is called once per frame
    void Update () { }
    public void BacktoMenuButtonClick (Button button) {
        SceneManager.LoadScene ("SecenMainmenu");
    } 
    public void ExitButtonClick (Button button) {
        Application.Quit ();
    }
}
