﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Traget : MonoBehaviour
{
    
    Vector3 _initialPosition;

    Vector3 unitCircleDirection;
  

    // Start is called before the first frame update
    void Start()
    {
        _initialPosition = this.transform.position;
    }


    // Update is called once per frame
    void Update()
    {

        unitCircleDirection = new Vector3(0,0,0);
        this.transform.position = _initialPosition + unitCircleDirection;

        if (Input.GetKey(KeyCode.LeftArrow))
            unitCircleDirection += new Vector3(-1, 0, 0); 
        if (Input.GetKey(KeyCode.RightArrow))
            unitCircleDirection += new Vector3(1, 0, 0);
        if (Input.GetKey(KeyCode.UpArrow))
            unitCircleDirection += new Vector3(0, 1, 0);
        if (Input.GetKey(KeyCode.DownArrow))
            unitCircleDirection += new Vector3(0, -1, 0);



    }
}
