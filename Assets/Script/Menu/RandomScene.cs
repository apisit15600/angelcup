﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RandomScene : MonoBehaviour
{
    public static RandomScene instance;
    [Header("Select of Teams")]
    public GameObject selectZeus ;
    public GameObject selectHades ;
    public GameObject selectPoseidon;
    public GameObject selectDemeter ;
    public void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    public void Update()
    {

    }
    public void ChangeScore(int Team)
    {
        if(Team == 0)
        {
           selectZeus.SetActive(true);
        }
    }
}
